DESCRIPTION = "The SECO Northern Europe Standard Yocto Image. \
It contains the base system plus Wayland, Qt5 and Pulseaudio."

#require seconorth-image.bb
require recipes-bsp/images/seconorth-image.bb

DISTRO_FEATURES += "pulseaudio"

IMAGE_INSTALL += "pulseaudio-server"
IMAGE_INSTALL += "pulseaudio-misc"
