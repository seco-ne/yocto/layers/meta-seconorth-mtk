FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/${LK_BOARD_NAME}:"

SRC_URI:append = " \
    file://lk.bin \
"

do_deploy:prepend() {
    # binary is located in genio-700-evk, so we need to copy it from there
    install -d ${BUILD}
    install -m 0755 ${WORKDIR}/lk.bin ${BUILD}/lk.bin
}
