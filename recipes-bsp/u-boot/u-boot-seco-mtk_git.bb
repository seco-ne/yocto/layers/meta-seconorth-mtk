require u-boot-common_${PV}.inc
require recipes-bsp/u-boot/u-boot.inc

PROVIDES += "u-boot"

UBOOT_INITIAL_ENV = "u-boot-initial-env"

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "seco-genio700|seco-genio510"
