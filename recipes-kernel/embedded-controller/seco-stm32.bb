SUMMARY = "${PN} STM32 driver."

require conf/seconorth-kernel-modules.inc

SRCREV = "${AUTOREV}"
SRC_URI = " \
    file://Makefile \
    git://git.seco.com/seco-ne/kernel/modules/seco-stm32.git;protocol=https;branch=main;nobranch=1;destsuffix=src/ \
"
