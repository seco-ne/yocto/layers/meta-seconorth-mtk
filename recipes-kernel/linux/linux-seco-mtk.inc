# Copyright (C) 2019 Fabien Parent <fparent@baylibre.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-kernel/linux/linux-yocto.inc

do_assemble_fitimage[depends] += "virtual/bootloader:do_populate_sysroot"

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

# Embed SRCREV into the version string;
# Use "linux-mtk+modified" if using "devtool modify linux-mtk"
LINUX_VERSION_EXTENSION = "-mtk+${@("modified" if d.getVar('EXTERNALSRC') else ("g" + d.getVar('SRCREV')[0:12]))}"

S = "${WORKDIR}/git"

SRC_URI:append = " \
	${@bb.utils.contains('MACHINE_FEATURES', 'optee', 'file://optee.cfg', '', d)} \
"

# Board specific config fragments
SRC_URI:append:genio700 = " file://genio700.cfg "
SRC_URI:append:genio510 = " file://genio510.cfg "
SRC_URI:append:genio = " file://genio-common.cfg "

# SoC specific config fragments
SRC_URI:append:mt8188 = " \
	file://mt8188.cfg \
	${@bb.utils.contains('MACHINE_FEATURES', 'tsn', 'file://tsn.cfg', '', d)} \
"

SRC_URI:append:mt8370 = " \
	file://mt8370.cfg \
	${@bb.utils.contains('MACHINE_FEATURES', 'tsn', 'file://tsn.cfg', '', d)} \
"

KERNEL_EXTRA_ARGS = "Image.gz dtbs"
KCONFIG_MODE = "--alldefconfig"
KBUILD_DEFCONFIG = "defconfig"

export DTC_FLAGS = '-@'

# branch= combined with nobranch=1 looks weird, but the branch is used for the
# AUTOREV, the nobranch tells the fetcher to not check the SHA validation for the branch
# This way the SRCREV can be specified to a specific revision also outside that branch
# with SRCREV:pn-linux-guf =
SRC_URI = "${SECO_KERNEL_URI}/linux-mtk.git;protocol=https;branch=${SRCBRANCH};nobranch=1"
PV = "${LINUX_VERSION}+git${SRCPV}"

uboot_prep_kimage() {
	linux_comp="gzip"
	cp arch/arm64/boot/Image.gz linux.bin
	echo ${linux_com}
}

do_bundle_initramfs:prepend() {
	if [ x"${@bb.utils.contains("DISTRO_FEATURES", "security", "1", "0", d)}" = x1 -a x"${INITRAMFS_IMAGE_BUNDLE}" = x1 ]; then
		echo "CONFIG_INITRAMFS_SOURCE=\"${B}/usr/${INITRAMFS_IMAGE_NAME}.cpio\" CONFIG_INITRAMFS_COMPRESSION_GZIP=y" >> ${B}/.config
	fi
}

COMPATIBLE_MACHINE_mediatek-bsp = "seconorth-machine"
