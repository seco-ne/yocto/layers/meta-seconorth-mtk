FILESEXTRAPATHS:prepend:genio := "${THISDIR}/genio:"

# KERNEL_DTBO list is defined in machine config
SRC_URI:append:genio = " \
    ${@' '.join(['file://' + file[:-5] + '.dts' for file in KERNEL_DTBO.split()])} \
"
