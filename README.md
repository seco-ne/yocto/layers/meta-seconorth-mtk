# meta-seconorth-mtk
See README.md in the ```manifest``` project for project information.

This layer is intended to hold all recipes needed to build the Board Support Package (BSP) for SECO Northern Europe MediaTek-based boards.

It does not provide a buildable image. Instead the necessary the Yocto MACHINE abstraction is provided that can be use dto build a custom distribution.

However, the primary use-case for this layer is the combination with the ```meta-seconorth-distro``` layer to create a SECO Northern Europe standard image build environment. See the ```manifest``` for details.

## License

This layer is licensed under the Apache License 2.0. See also ```LICENSE.txt``` inside the root directory.
Recipes with deviating licenses have ```LICENSE.txt``` inside the recipe directories.

