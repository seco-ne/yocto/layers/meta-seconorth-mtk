# Remove networkd configs added by meta-rity layer
do_install:append() {
    rm -rf ${D}${sysconfdir}/systemd/network/
}
