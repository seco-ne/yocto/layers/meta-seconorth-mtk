FILESEXTRAPATHS:append := "${THISDIR}/${PN}:"

SRC_URI += " \
    file://dma_heap.rules \
    file://91-can_interface.rules \
"

do_install:append() {
    install -m 0644 ${WORKDIR}/dma_heap.rules   ${D}${sysconfdir}/udev/rules.d/dma_heap.rules
    install -m 0644 ${WORKDIR}/91-can_interface.rules   ${D}${sysconfdir}/udev/rules.d/91-can_interface.rules
}
